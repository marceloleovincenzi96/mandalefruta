var express = require('express');
var router = express.Router();
var productos_mod = require("../models/productos_models");
var db = require("../bin/mysql");

var p = new productos_mod();
var ultimaId = 0;

/* GET home page. */
router.get('/obtenerprod', function (req, res, next) {


    p.getProducto().then(resultado => {
            res.json(resultado)
        })
        .catch((err) => setImmediate(() => {
            throw err;
        }));

});

router.get('/obtenerped/:id/', function (req, res, next) {
    var id = req.params.id

    var query = db.query("SELECT * FROM pedido WHERE id_usuario = ? ORDER BY id DESC LIMIT 10", [id], function (err, result) {
        if (err) {

            console.log(err);

            throw err;

        } else {

            return res.json(result);
        }

    })

})

router.get('/total/:id_usr', function (req, res, next) {
    var id_usr = req.params.id_usr

    var query = db.query("SELECT valor FROM detalle INNER JOIN pedido ON detalle.id_pedido = pedido.id WHERE id_usuario = ? ORDER BY id_pedido DESC LIMIT 10 ", [id_usr], function (err, result) {
        if (err) {

            console.log(err);

            return reject(err);

        } else {

            console.log(result);

            return res.json(result);
        }

    })

})

router.get('/registrarped/:id_usuario', function (req, res, next) {

    var id_usr = req.params.id_usuario;
    var datetime = new Date();

    var query = db.query('insert into pedido (id_usuario, fecha) values(?,?)', [id_usr, datetime], function (error, result) {
        
        
        if (error) {
            throw error;

        } else {
            ultimaId = result.insertId;
            console.log(result);
            console.log("Pedido registrado exitosamente")
            console.log(ultimaId);
            return res.json(result);
        }
    });

})

router.get('/registrardet/:idprod/:precioprod/:cantidad', function (req, res, next) {

    var idProd = req.params.idprod;
    var precioProd = req.params.precioprod;
    var cantidadPed = req.params.cantidad;


    setTimeout (function(){
        var query = db.query('insert into detalle (id_pedido, id_productos, cantidad, valor) values(?, ?, ?, ?)', [ultimaId, idProd, cantidadPed, precioProd * cantidadPed], function (error, result) {
        if (error) {
            throw error;

        } else {
            console.log(result);
            console.log("Detalle registrado exitosamente")
            res.json(result);
        }
    
    
    })}, 3000);
})



module.exports = router;