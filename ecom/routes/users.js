var db = require("../bin/mysql");
var express = require('express');
var usuarios_mod = require("../models/usuarios_models");
var router = express.Router();
var jwt = require('jsonwebtoken');

var u = new usuarios_mod();


/* GET users listing. */
router.post('/autenticacion', function (req, res, next) {
  u.getUsuario(req.body.email, req.body.password).then(function(userInfo){
    
      if (userInfo.length <= 0 || !userInfo){
        res.json({status:"not_found", message: "user not found!!!", data:null});
      } else {
          console.log(userInfo[0].id);
          const token = jwt.sign({id: userInfo[0].id}, req.app.get('secretKey'), { expiresIn: '1h' });
          res.json({status:"success", message: "user found!!!", usuario: userInfo[0].usuario, id: userInfo[0].id, data:{user: userInfo, token:token}});
    }
  
  }).catch((err) => setImmediate(() => {
    console.log("asdasd");
    throw err;
  }));

});



router.post('/registro', function (req, res, next) {

  console.log(req.body);

  console.log("Ha entrado: " + req.body.user);

  if (req.body.password == req.body.repeat_password) {

    var query = db.query('insert into usuarios(usuario,email,password) values(?,?,?)', [req.body.user, req.body.email, req.body.password], function (error, result) {
      if (error) {
        throw error;

      } else {
        console.log(result);
      }
    });

    console.log("Se ha registrado con exito");


  } else {

    console.log("Los passwords no coinciden");

  }

});


module.exports = router;